import React from 'react';
import Board from "../components/snake/Board";
import Settings from "../components/Settings";
import {Box, Grid} from "@material-ui/core";

const index = () => {
  return (
    <Box
      display={"flex"}
    >
      <Grid container>
        <Grid item xs={3}>
          <Settings/>
        </Grid>
        <Grid item xs={9} style={{height: "100vh"}}>
          <Box
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            height={"100%"}
          >
          <Board/>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default index;