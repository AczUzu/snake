import React from 'react';
import Layout from "./Layout";
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import SnakeThemeJson from "./SnakeThemeJson";

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    snake: {
      color1: string,
      color2: string,
      speed: number,
    },
    board: {
      color: string,
      borderColor: string,
      edgeLength: number,
      squareEdgeLength: number,
    },
    food: {
      color: string,
      spawnSpeed: number,
    },
    subscribe: Function,
  }

  // allow configuration using `createMuiTheme`
  // interface ThemeOptions {
  //   status?: {
  //     danger?: string;
  //   };
  // }
}
const theme = createMuiTheme(SnakeThemeJson as any);

function App() {
  return (
    <div>
      <ThemeProvider theme={theme}>
        <Layout/>
      </ThemeProvider>
    </div>
  )
    ;
}

export default App;
