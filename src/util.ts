import {EventEmitter} from "events";

type Listener = (...args: any[]) => any;
const ee = new EventEmitter();
const THEME_UPDATE_EVENT = '__theme-updated__'
export const wrapWithProxy = (target: any, propsToEmitEvent: string[]) =>
  new Proxy(target, {
    set(target: any, p: string, value: any, receiver: any): boolean {
      if ([...propsToEmitEvent].includes(p)) ee.emit(THEME_UPDATE_EVENT);
      target[p] = value;
      return true;
    },
  });

export const subscribe = (cb: Listener) => {
  ee.on(THEME_UPDATE_EVENT, cb);
  return () => ee.removeListener(THEME_UPDATE_EVENT, cb);
}
