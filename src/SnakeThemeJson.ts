import {subscribe, wrapWithProxy} from "./util";


const snakeTickDelay = 200;
const edgeLength = 30;
const Settings = {
  board: {
    color: "#03a9f4",
    borderColor: "#0276aa",
    edgeLength: edgeLength,
    squareEdgeLength: 30,
  },
  snake: {
    color1: "#b23c17",
    color2: "#cddc39",
    speed: snakeTickDelay,
  },
  food: {
    color: "#ff5722",
    spawnSpeed: snakeTickDelay * edgeLength,
  },
  subscribe: subscribe,
};

Settings.board = wrapWithProxy(Settings.board, ["color", "borderColor"]);
Settings.food = wrapWithProxy(Settings.food, ["color"]);
export default Settings