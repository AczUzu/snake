import {Square} from "../../gameLogic/Square";

export default class Food {

  getRandomInt(max: number) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  randomSquare: Square;

  constructor(private f: () => Square[]) {
    const availableSquares = this.f();
    const randomIndex = this.getRandomInt(availableSquares.length);

    this.randomSquare = availableSquares[randomIndex];
  }

  setFood() {
    this.randomSquare.setFood();
  }

  setEmptyFood() {
    this.randomSquare.setEmpty();
  }

}
