import React, {useEffect, useState} from 'react';
import Tile from "./Tile";
import {Box, useTheme} from "@material-ui/core";
import GameLogic from "../../gameLogic/GameLogic";
import GameOver from "../GameOver";


const game = new GameLogic();

const Board = () => {
  const theme = useTheme();
  const [finished, setFinished] = useState(false);
  const [dump, setDump] = useState(0);
  const reRender = () => setDump(dump + 1);

  game.snakeSpeedMs = theme.snake.speed;
  game.foodSpawnMs = theme.food.spawnSpeed;
  game.edgeLength = theme.board.edgeLength;
  game.onGameOver = () => setFinished(true);
  game.start();

  useEffect(() => {
    return theme.subscribe(reRender);
  });

  return (
    <Box>
      <Box
        display={finished ? "none" : "flex"}
        flexDirection={"column"}
      >
        {
          game.getBoardState().map((row, i) => (
            <Box key={`row${i}`} display={"flex"}>
              {row.map(sqr => (<Tile key={sqr.idX} square={sqr} defaultBgColor={theme.board.color}/>))}
            </Box>
          ))
        }
      </Box>

      {finished ? <GameOver/> : null}

    </Box>
  );
};

export default Board;