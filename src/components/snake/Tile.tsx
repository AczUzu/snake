import React, {useState} from 'react';
import {Box, useTheme} from "@material-ui/core";
import {Square} from "../../gameLogic/Square";

type P = {
  square: Square,
  defaultBgColor: string,
}


const Tile: React.FC<P> = ({square, defaultBgColor}) => {
  const {board, snake, food} = useTheme();

  const edge = board.squareEdgeLength;
  const borderColor = board.borderColor
  const textColor = "black"
  const snakeColor = snake.color1
  const snake2Color = snake.color2
  const foodColor = food.color

  const colorJson: any = {
    "snake": snakeColor,
    "snake2": snake2Color,
    "food": foodColor,
    "empty": defaultBgColor
  }

  const [fake, setFake] = useState(0);
  square.setRenderer(() => setFake(fake + 1));

  return (
    <Box
      height={edge}
      width={edge}
      border={`${square.type === "empty" ? "solid" : "dotted"} 1px ${borderColor}`}
      textAlign={"center"}
      fontSize={"5px"}
      bgcolor={colorJson[square.type]}
      color={textColor}>
      {/*<div>{square.idX} - {square.idY}</div>*/}
      {/*{square.type}*/}
    </Box>
  );

};


export default Tile;