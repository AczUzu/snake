import React, {useState} from 'react';
import {Box, useTheme} from "@material-ui/core";
import {ColorPicker as PlainColorPicker} from "material-ui-color";


const ColorPicker = ({label, defaultColor, onChange}: { label: string, defaultColor: string, onChange: (c: string) => any }) => {
  const [color, setColor] = useState(defaultColor);
  return (
    <>
      <h5>{label}</h5>
      <PlainColorPicker value={color} onChange={(color) => {
        setColor(`#${color.hex}`);
        onChange(`#${color.hex}`);
      }}/>
    </>
  );
}

const Settings = () => {
  const {snake, food, board} = useTheme();

  return (
    <Box
      display={"flex"}
      alignItems={"center"}
      borderRight={"dotted 1px"}
      // borderRadius={"20px"}
      width={"100%"}
      height={"100%"}
    >
      <ul style={{margin: "auto", listStyleType: "none", padding: 0}}>
        <li>
          <ColorPicker
            label={'Snake1 color'}
            defaultColor={snake.color1}
            onChange={(color = snake.color1) => snake.color1 = color}
          />
        </li>
        <br/>
        <li>
          <ColorPicker
            defaultColor={snake.color2}
            label={"Snake2 color"}
            onChange={(color = snake.color2) => snake.color2 = color}
          />
        </li>
        <br/>
        <li>
          <ColorPicker
            defaultColor={board.color}
            label={"Tile color"}
            onChange={(color = board.color) => board.color = color}
          />
        </li>
        <br/>
        <li>
          <ColorPicker
            defaultColor={board.borderColor}
            label={"Tile border color"}
            onChange={(color = board.borderColor) => board.borderColor = color}
          />
        </li>
        <li>
          <ColorPicker
            defaultColor={food.color}
            label={"Food color"}
            onChange={(color = food.color) => food.color = color}
          />
        </li>
      </ul>
    </Box>
  );
};

export default Settings;