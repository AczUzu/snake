import React from 'react';
import {Box} from "@material-ui/core";

const GameOver = () => {
  return (
    <Box
      position={"absolute"}
      top={0}
      left={0}
      width={"100%"}
      height={"100%"}
      bgcolor={"#b71c1c"}
      color={"#fff"}
      display={"flex"}
      justifyContent={"center"}
      alignItems={"center"}
      zIndex={999}
    >
      <h1>Game Over</h1>
    </Box>

  );
};

export default GameOver;