export default class {
  private intervalRef: any;

  constructor(private cb: Function, private intervalDelay = 50) {
    this.start();
  }

  start() {
    if (this.intervalRef) return;
    this.intervalRef = setInterval(this.cb, this.intervalDelay);
  }

  stop() {
    clearInterval(this.intervalRef);
    this.intervalRef = null;
  }

  setInterval(n: number) {
    this.intervalDelay = n;
    this.stop();
    this.start();
  }
}