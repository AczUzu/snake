import Snake from "./Snake";
import {Square} from "./Square";
import Food from "../components/snake/Food";
import Timer from "./Timer";

const noop = () => {}; // means no operation

export default class GameLogic {
  private moveTimer: Timer;
  private foodTimer: Timer;

  private _edgeLength: number = 0;
  private isStarted = false;

  private idX = 0;
  private idY = 0;
  private board: Square[][] = [];
  private snakes: Snake[] = [];
  private food?: Food;
  public onGameOver: Function = noop;

  constructor() {
    this.moveTimer = new Timer(this.tickMove);
    this.foodTimer = new Timer(this.tickFood);
  }

  start() {
    if (this.isStarted) return;
    this.isStarted = true

    this.fillBoardArray();
    this.createRow();
    this.createSnakes();

    this.createFood();
    this.moveTimer.start();
    this.foodTimer.start();

    this.registerMovementCallbacks();
  }

  pause() {
    this.moveTimer.stop();
    this.foodTimer.stop();
  }

  private registerMovementCallbacks() {
    const snakeDirJson: any = [
      {ArrowLeft: "left", ArrowUp: "up", ArrowRight: "right", ArrowDown: "down"},
      {a: "left", w: "up", d: "right", s: "down"},
    ]

    window.onkeydown = (e: KeyboardEvent) => {
      this.snakes.forEach((snk, i) => {
        const directionMapping = snakeDirJson[i];
        const isMovementRelatedWithThisSnake = Object.keys(directionMapping).includes(e.key.toString());

        if (isMovementRelatedWithThisSnake) snk.setDirection(directionMapping[e.key]);
      });
    }
  }

  private createSnakes() {
    const idToSquare = (cd: any) => this.getSquareByIds(cd.x, cd.y);
    this.snakes = [
      new Snake([{x: 4, y: 4}, {x: 4, y: 5}, {x: 4, y: 6}].map(idToSquare), "snake"),
      new Snake([{x: 4, y: 14}, {x: 4, y: 15}, {x: 4, y: 16}].map(idToSquare), "snake2"),
    ];
    this.snakes.forEach(snk => {
      snk.onMoveOut = () => this.finishGame()
    });
  }

  set snakeSpeedMs(value: number) {
    this.moveTimer.setInterval(value);
  }

  set foodSpawnMs(value: number) {
    this.foodTimer.setInterval(value);
  }

  set edgeLength(value: number) {
    this._edgeLength = value;
  }

  private createFood() {
    this.food && this.food.setEmptyFood();
    this.food = new Food(() => this.returnAvailableSquares());
    this.food.setFood();
  }

  tickFood = () => {
    this.createFood();
  }

  tickMove = () => {
    this.snakes.forEach(snk => {
        snk.move()
        this.snakes.forEach(other => {
          if (snk !== other && snk.contains(other)) this.finishGame();
        });
      }
    );
  }

  private finishGame() {
    this.pause();
    this.onGameOver();
  }

  fillBoardArray = () => {
    let i = 0;
    while (i < this._edgeLength) {
      this.board.push([]);
      i++;
    }
  }

  createRow = () => {
    let i = 0;
    while (i < this._edgeLength) {
      this.fillRow(i);
      this.idY++;
      this.idX = 0;
      i++;
    }
  }

  private fillRow(i: number) {
    let a = 0;
    while (a < this._edgeLength) {
      this.board[i].push(this.createSquareData(i));
      a++;
    }
  }


  returnAvailableSquares(): Square[] {
    let availableSquares: any = [];
    for (let a of this.board) {
      for (let b of a) {
        if (this.getSquareByIds(b.idX, b.idY).type === "empty") {
          availableSquares.push(this.getSquareByIds(b.idX, b.idY))
        }
      }
    }
    return availableSquares;
  }

  private createSquareData(i: number): Square {
    const idX = this.idX;
    const newSqr = new Square(
      this.idX,
      i,
      (dir) => {
        const squareCoords = {
          "top": {x: idX, y: i - 1},
          "right": {x: idX + 1, y: i},
          "bottom": {x: idX, y: i + 1},
          "left": {x: idX - 1, y: i},
          "invalid": {x: idX, y: i},
        }[dir || "invalid"];

        return this.getSquareByIds(squareCoords.x, squareCoords.y);
      },
    );

    this.idX++;
    return newSqr;
  }

  getSquareByIds = (idX: number, idY: number) => {
    return this.board[idY] && this.board[idY][idX];
  }

  getBoardState = () => {
    return this.board;
  }
}