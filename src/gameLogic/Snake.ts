import {Square} from "./Square";

class Snake {
  private lastMovementDirection: string = "";
  private direction: string = "right";
  public onMoveOut: Function = () => {
  };

  constructor(
    private squares: Square[],
    private snakeColor: "snake" | "snake2"
  ) {
    this.squares.forEach(sqr => sqr.setSnake(this.snakeColor));
  }

  setDirection(a: string) {
    const reverse: any = {
      "up": "down",
      "right": "left",
      "down": "up",
      "left": "right",
    }

    if (this.lastMovementDirection === reverse[a]) return;

    this.direction = a;
  }

  head() {
    return this.squares[this.squares.length - 1];
  }

  tail() {
    return this.squares[0];
  }

  drawSnake() {
    this.squares.forEach(sqr => {
      sqr.setSnake(this.snakeColor)
    })
  }

  //
  // isOutBound = (edgeLength: number) => {
  //   const {idX, idY} = this.head();
  //
  //   return !(idX && idY);
  // }

  contains(snk: Snake): boolean {
    for (let b of this.squares.slice(0, this.squares.length - 1)) {
      if (this.head() === b) {
        return true;
      }
    }
    for (let a of snk.squares) {
      if (this.head() === a) {
        return true;
      }
    }
    return false;
  }

  move() {
    const nextSquare = this.getNextSquare(this.direction);

    if (nextSquare === undefined) {
      this.onMoveOut();
      return;
    }

    this.grow(nextSquare);
    this.squares.push(nextSquare)
    this.lastMovementDirection = this.direction;
    this.drawSnake();
  }

  private grow(nextSquare: Square) {
    const isFood = nextSquare.type === "food";
    if (!isFood) {
      this.tail().setEmpty();
      this.squares.shift()
    }
  }

  private getNextSquare(dir: string) {
    const squareGetters: { [k: string]: () => Square } = {
      "up": () => this.head().getTopSquare(),
      "right": () => this.head().getRightSquare(),
      "down": () => this.head().getBottomSquare(),
      "left": () => this.head().getLeftSquare(),
    };
    return squareGetters[dir]();
  }
}

export default Snake;