export class Square {
  public type: "empty" | "snake" | "food" | "snake2" = "empty";
  private render: Function = () => {
  };

  constructor(
    public idX: number,
    public idY: number,
    private squareGetter: (direction?: "top" | "right" | "bottom" | "left") => Square,
  ) {
  }

  setEmpty() {
    this.type = "empty";
    this.render();
  }

  setSnake(a: "snake" | "snake2") {
    this.type = a;
    this.render();
  }

  setFood() {
    this.type = "food";
    this.render();
  }

  getTopSquare() {
    return this.squareGetter("top");
  }

  getRightSquare() {
    return this.squareGetter("right");
  }

  getBottomSquare() {
    return this.squareGetter("bottom");
  }

  getLeftSquare() {
    return this.squareGetter("left");
  }

  setRenderer(renderer: Function) {
    this.render = renderer;
  }
}