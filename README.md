This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Demo
https://portfolio-32161.web.app/

## Video
https://www.youtube.com/watch?v=7zMAkISfSWQ

[![Alt text](https://img.youtube.com/vi/7zMAkISfSWQ/0.jpg)](https://www.youtube.com/watch?v=7zMAkISfSWQ)


## Description
This is a sample project to show my work. I have implemented a snake game can very easily configurable and I also I cared about the performance of my work. My app can handle 40x40 grid which means 1600+ react components. Also be aware the app needs to be re-rendered multiple times in a second (5 times/sec in the video). I have eliminated unnecessary renders to boos the performance of my app.